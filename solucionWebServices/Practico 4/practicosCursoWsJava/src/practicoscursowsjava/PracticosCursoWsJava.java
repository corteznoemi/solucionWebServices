/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicoscursowsjava;

/**
 *
 * @author anonimo
 */
public class PracticosCursoWsJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Práctico 2---------");
        practico2();
        System.out.println("Práctico 3---------");
        practico3();
    }
    
    
    public static void practico2 (){
        System.out.println("  ¡Hola Mundo!");
    }
    
    public static void practico3(){
        int var1= 15;
        int var2= 8;
        int v_suma=var1 + var2;
        int v_resta=var1-var2;
        int v_multiplicacion =var1*var2;
        float v_division=var1/var2;
        int v_resto=var1%var2;
        
        
        System.out.println(" Suma: " + v_suma); 
        System.out.println(" Resta: " + v_resta);
        System.out.println(" Multiplicación: " + v_multiplicacion);
        System.out.println(" División: " + v_division);
        System.out.println(" Modulo: " + v_resto);
       
    }
}
