/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio.pkg1;

/**
 *
 * @author anonimo
 */
public class Vectores {
    
    private int vecPositivos[]=new int[10];
    private int vecNegativos[]=new int[10];
    private int sumPositivos;
    private int sumNegativos;
    
    
    public Vectores(int vectorPpal[]){
        int indPos=0;
        int indNeg=0;
        for(int i=0; i<vectorPpal.length;i++){
            if (vectorPpal[i]>=0){
                vecPositivos[indPos]=vectorPpal[i];
                indPos++;
                sumPositivos+=vectorPpal[i];
            }
            else{
                vecNegativos[indNeg]=vectorPpal[i];
                indNeg++;
                sumNegativos+=vectorPpal[i];
            }
            
        }
    }

    @Override
    public String toString() {
        return "Vectores{" + "vecPositivos=" + vecPositivos + ", vecNegativos=" + vecNegativos + ", sumPositivos=" + sumPositivos + ", sumNegativos=" + sumNegativos + '}';
    }

    public int[] getVecPositivos() {
        return vecPositivos;
    }

    public int[] getVecNegativos() {
        return vecNegativos;
    }

    public int getSumPositivos() {
        return sumPositivos;
    }

    public int getSumNegativos() {
        return sumNegativos;
    }
    
    
    
    
}
