/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio.pkg1;

/**
 *
 * @author anonimo
 */
public class Imprimir {
    
    public static void mostrar(Vectores vec, VectorPpal vppal){
        int vpal[]=vppal.getVecNumeros();
        int vec1[]=vec.getVecPositivos();
        int vec2[]=vec.getVecNegativos();
        
        System.out.println("Vector de numeros principal");
        for(int i=0; i<vpal.length;i++){
            System.out.println("VecNros[" + i + "]: " + vpal[i]);
            
        }
        System.out.println("Vector de numeros positivos");
        for(int i=0; i<vec1.length;i++){
            System.out.println("VecPos[" + i + "]: " + vec1[i]);
            
        }
        
        System.out.println("Vector de numeros negativos");
        for(int i=0; i<vec2.length;i++){
            System.out.println("VecNeg[" + i + "]: " + vec2[i]);
            
        }
        
        System.out.println("Total de la suma de nros positivos:" + vec.getSumPositivos());
        System.out.println("Total de la suma de nros negativos:" + vec.getSumNegativos());
        System.out.println("Total de la suma de todos los nros:" + (vec.getSumPositivos() + vec.getSumNegativos()));
    }
    
}
